We thank all contributors of this project!

# Developers
- Markus Katharina Brechtel (Working Group Cohorts in Infectious Diseases and Oncology; University Hospital Cologne / University Hospital Frankfurt)
- Philipp K. (Ghostroute Consulting)
- Elias Hamacher (Working Group Cohorts in Infectious Diseases and Oncology; University Hospital Cologne)
