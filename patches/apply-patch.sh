#!/bin/sh

NC_VERSION=$1

# get path of current script
PATCH_PATH=$(dirname $(realpath $0))

PATCH_FILE=$(ls -1 "$PATCH_PATH/nextcloud-header-injection-$NC_VERSION".*.diff | sort -n | tail -n1)

# check if there is a string "rps-header" in core/templates/layout.user.php
if grep -q "rps-header" core/templates/layout.user.php; then
    echo "Patch already applied"
    exit 0
fi

echo "Applying patch $PATCH_FILE"

exec patch -p1 -i $PATCH_FILE
