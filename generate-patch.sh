#!/bin/sh
#
# Generate version-specific patch from git repository, and store in patches/
#

V="$1"
FN="patches/nextcloud-header-injection-$V.diff"

[ -z "$V" ] && echo "Usage: $0 <Version_Number_as_in_git_tags>" >&2 && exit 1

git diff tags/"$V"..nextcloud/"$V" > "$FN"
E=$?
[ "$E" -ne 0 ] && rm "$FN"
[ "$E" -eq 0 ] && git add "$FN"
exit "$E"
