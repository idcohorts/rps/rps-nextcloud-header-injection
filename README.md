# RPS Nextcloud header injection

This repo is used to clone the nextcloud server and modify the templates and track the changes in git.

To include the RPS header you need to insert two html tags into the nextcloud templates and add a class to the nextcloud body tag.:

- Script loading somewhere in the `<head>` area: `<script type="text/javascript" src="js/header.js"></script>`
- Add a class `rps-nextcloud` to the `<body>` element: `<body id="body-user" class="rps-nextcloud"` ...
- Header node directly at the beginin of the pages `<body>` element: `<nav id="rps-header" class="rps-header"></nav>`

The actual code is only in the branches for the respective Nextcloud version. The idea is to track our changes as a commit on the nextcloud server repo.

The `rps-apps` ansible role for deploying nextcloud however does not like checking out a multi-gigabyte repository on each build.
As such, on this `main` branch, there exists a `patches/` folder with an out-generated diff for each supported nextcloud version.

## Setup dev env

- Clone this repo as usual
- Add the upstream repo: `git remote add upstream https://github.com/nextcloud/server.git`
- Set the upstream remote to only fetch version tags: `git config --local remote.upstream.fetch "+refs/tags/v*:refs/tags/v*"`
- Fetch the upstream repo release tags: `git fetch upstream --tags` (this will download all commits and therefore take a while )

## Initial modifications

This is how I did the initial modifications. You probably don't need to
do this part again, as `git cherry-pick` is pretty good at applying old
patches to a changing code bases. So continue with the "Update the branch
to the next upstream release" section, and only circle back if necessary.

### Create a new branch for a new nextcloud version

- Create a new branch from the upstream tag: `git checkout -b nextcloud/v25.0.0 v25.0.0`

### Make your changes
- Go to core/templates and insert the header tags into the templates
- Commit your changes: `git commit -am "Add RPS header to templates"`
- Create a branch for the minor version: `git branch nextcloud/v25.0`
- Push the new branches to origin: `git push -u origin nextcloud/v25.0 nextcloud/v25.0.0`

## Update to the next upstream release
- Fetch the upstream repo release tags: `git fetch upstream --tags`
- Create a new branch from the upstream tag: `git checkout -b nextcloud/v25.0.3 v25.0.3`
- Cherry-pick the change commit from the previous branch: `git cherry-pick nextcloud/v24.0`
- Fix any merge conflicts and commit them
- review your commit: `git diff HEAD^ HEAD`
- Delete the old minor version branch, if it already exists: `git branch -d nextcloud/v25.0`
- Create a branch new for the minor version: `git branch nextcloud/v25.0`
- Push the new branch to origin: `git push -u origin --force nextcloud/v25.0.3 nextcloud/v25.0`

## Check if you need to update the minor version branch
- Fetch the upstream repo release tags: `git fetch upstream --tags`
- Compare the new version with the old one `git diff v25.0.3 v25.0.4 -- core/templates/layout.user.php`
- When there are changes, update to the next upstream release like described above

## Push out a stand-alone Diff file to the `main` branch
- `git checkout main`
- `git pull` if necessary
- run the `generate-patch` script with the version you worked on:
  * `./generate-patch.sh v25.0.4`
- `git commit`

Thank you for keeping our header working for another version! :)
